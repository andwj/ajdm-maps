//
// Ritual shaders
//

textures/aj_ritual/sky
{
	skyParms full 1280 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_globaltexture
	q3map_sunExt   0.5 0.75 1  20  0 90  12 16
	q3map_lightRGB 0.5 0.75 1
	q3map_skylight 70 16

	qer_editorimage textures/aj_ritual/sky_clouds.tga
	{
		map textures/aj_ritual/sky_stars
		tcMod scale 3 3
	}
	{
		map textures/aj_ritual/sky_clouds
		tcMod scroll 0.02 0.07
		blendFunc add
	}
}

textures/aj_ritual/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 1.0 0.9 0.7
	q3map_surfacelight 200

	qer_editorimage textures/aj_ritual/tele1.jpg
	{
		map textures/aj_ritual/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_ritual/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_ritual/tlamp
{
	surfaceparm nolightmap

	q3map_lightRGB 1.00 0.9 0.7
	q3map_surfacelight 5000

	qer_editorimage textures/aj_ritual/tlamp.tga

	{
		map textures/aj_ritual/tlamp
		rgbGen const ( 0.7 0.5 0.3 )
	}
}

textures/aj_ritual/blamp
{
	surfaceparm nolightmap

	q3map_lightRGB 0.5 0.75 1
	q3map_surfacelight 5000

	qer_editorimage textures/aj_ritual/blamp.tga

	{
		map textures/aj_ritual/blamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_ritual/rlamp1
{
	q3map_lightRGB 1.00 0.9 0.7
	q3map_surfacelight 3000

	qer_editorimage textures/aj_ritual/rlamp.tga

	{
		map textures/aj_ritual/rlamp.tga
		rgbGen const ( 0.6 0.6 0.6 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_ritual/rlamp_glow.tga
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_ritual/rlamp2
{
	q3map_lightRGB 1.00 0.9 0.7
	q3map_surfacelight 6000

	qer_editorimage textures/aj_ritual/rlamp.tga

	{
		map textures/aj_ritual/rlamp.tga
		rgbGen const ( 0.6 0.6 0.6 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_ritual/rlamp_glow.tga
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_ritual/lamp8
{
	qer_editorimage textures/aj_ritual/lamp8.jpg

	q3map_lightRGB 1 1 1
	q3map_surfacelight 2000

	{
		map textures/aj_ritual/lamp8
		rgbGen identity
	}
	{
		map $lightmap 
		blendfunc filter
	}
	{
		map textures/aj_ritual/lamp8_glow
		blendfunc add
	}
}
