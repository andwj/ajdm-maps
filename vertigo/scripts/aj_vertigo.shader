//
// Vertigo shaders
//

textures/aj_vertigo/sky
{
	skyParms - 800 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 1.00 0.90 0.80
	q3map_skylight 400 16
	q3map_globaltexture

	qer_editorimage textures/aj_vertigo/sky1.jpg
	{
		map textures/aj_vertigo/sky1
		tcMod scroll 0.026 0.036
		depthWrite
	}
	{
		map textures/aj_vertigo/sky1
		tcMod scroll 0.02 0.02
		tcMod scale 2 2
		blendFunc add
	}
}

textures/aj_vertigo/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 0.25 0.5 1.0
	q3map_surfacelight 300

	qer_editorimage textures/aj_vertigo/tele1.jpg
	{
		map textures/aj_vertigo/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_vertigo/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_vertigo/water
{
	cull none

	surfaceparm water
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap

	qer_editorimage textures/aj_vertigo/water.jpg
	qer_trans 0.333

	{
		map textures/aj_vertigo/water
		rgbGen const ( 0.0625 0.0625 0.0625 )
//		tcmod turb .05 -0.5 0 0.02
		tcmod scroll .0641 -.01
		blendFunc add
	}
	{
		map textures/aj_vertigo/water
		rgbGen const ( 0.0625 0.0625 0.0625 )
//		tcmod turb .05 -0.5 0 0.02
		tcmod scroll .09 .022
		blendFunc add
	}
}

textures/aj_vertigo/lamp0
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000

	qer_editorimage textures/aj_vertigo/lamp1.jpg
	{
		map textures/aj_vertigo/lamp1
	}
	{
		map textures/aj_vertigo/lamp1_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_vertigo/lamp1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000

	qer_editorimage textures/aj_vertigo/lamp1.jpg
	{
		map textures/aj_vertigo/lamp1
	}
	{
		map textures/aj_vertigo/lamp1_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_vertigo/lamp2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 6000

	qer_editorimage textures/aj_vertigo/lamp1.jpg
	{
		map textures/aj_vertigo/lamp1
	}
	{
		map textures/aj_vertigo/lamp1_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_vertigo/lamp3
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 12000

	qer_editorimage textures/aj_vertigo/lamp1.jpg
	{
		map textures/aj_vertigo/lamp1
	}
	{
		map textures/aj_vertigo/lamp1_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}
