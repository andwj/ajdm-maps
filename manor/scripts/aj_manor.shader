//
// Manor shaders
//

textures/aj_manor/sky
{
	skyparms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_sun 1.00 0.80 0.60 300 150 52
	q3map_lightRGB 0.9 0.9 1.0
	q3map_skyLight 100 9

	qer_editorimage textures/aj_manor/sky1.tga
	{
		map textures/aj_manor/sky1.tga
		tcmod scale 2 3
		tcmod scroll 0.03 0.03
	}
	{
		map textures/aj_manor/sky2.tga
		blendfunc add
		tcmod scale 3 2
		tcmod scroll 0.015 0.015
	}
}

textures/aj_manor/lavahell
{
	cull disable

	surfaceparm lava
	surfaceparm fog
	surfaceparm trans
	surfaceparm lightfilter
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nodrop

	fogparms ( 0.929412 0.147059 0.147216 ) 912

	deformVertexes wave 256 sin 0 7 0 0.4
	tessSize 128

	q3map_lightRGB 1.0 0.2 0.1
	q3map_surfacelight 200
	q3map_globaltexture

	qer_editorImage textures/aj_manor/lavafloor.tga
	{
		map textures/aj_manor/lavafloor
		tcMod turb 0 0.1 0 0.1
		tcMod scroll 0.1 0.2
	}
	{
		map textures/aj_manor/lavafloor
		blendfunc add
		tcMod turb 0 0.2 0 0.1
		tcMod scroll -0.05 -0.02
	}
	{
		map textures/aj_manor/lavafloor
		blendfunc add
		tcMod turb 1 -0.3 0 0.07
		tcMod scroll 0.08 -0.08
	}
}

textures/aj_manor/mirror
{
	portal

	surfaceparm nolightmap

	qer_editorimage textures/aj_manor/conc-white.jpg
	qer_trans 0.5
	{
		map textures/aj_manor/invisible.tga
		blendfunc GL_ONE GL_ONE_MINUS_SRC_ALPHA
		depthWrite
	}
}

textures/aj_manor/evilwindow
{
	q3map_lightRGB 1.00 0 0
	q3map_surfacelight 150

	qer_editorimage textures/aj_manor/evilwindow.jpg
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_manor/evilwindow.jpg
		blendFunc filter
		rgbGen identity
	}
	{
		map textures/aj_manor/evilwindow_glow.jpg
		blendfunc add
	}
}

textures/aj_manor/painting1
{
	surfaceparm nonsolid

	qer_editorimage textures/aj_manor/painting1.jpg

	{
		map textures/aj_manor/painting1.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_manor/painting2
{
	surfaceparm nonsolid

	qer_editorimage textures/aj_manor/painting2.jpg

	{
		map textures/aj_manor/painting2.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_manor/painting3
{
	surfaceparm nonsolid

	qer_editorimage textures/aj_manor/painting3.jpg

	{
		map textures/aj_manor/painting3.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_manor/hanglamp
{
	qer_editorimage textures/aj_manor/hanglamp.tga

	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_manor/hanglamp.tga
		blendFunc filter
		rgbGen identity
	}
	{
		map textures/aj_manor/hanglamp_glow.tga
		blendfunc add
	}
}

textures/aj_manor/fluoro1
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 1000
	qer_editorimage textures/aj_manor/fluoro.jpg
	{
		map textures/aj_manor/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_manor/fluoro_glow.jpg
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_manor/fluoro2
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_manor/fluoro.jpg
	{
		map textures/aj_manor/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_manor/fluoro_glow.jpg
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_manor/cross
{
	q3map_lightRGB 1 0.7 0.3
	q3map_surfacelight 2500

	qer_editorimage textures/aj_manor/cross.jpg
	{
		map textures/aj_manor/cross
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_manor/cross_glow
		rgbGen wave sin 0.3 0.3 0 0.5
		blendFunc add
	}
}

textures/aj_manor/gold
{
	qer_editorimage textures/aj_manor/gold.jpg

	{
		map textures/aj_manor/goldfx.jpg
		rgbGen const ( 0.7 0.7 0.7 )
		tcGen environment
	}
}
