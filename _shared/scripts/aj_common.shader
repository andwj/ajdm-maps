//
// Common shaders
//

textures/common/caulk
{
	surfaceparm nodraw
	surfaceparm nomarks
	surfaceparm nolightmap
}

textures/common/clip
{
	surfaceparm playerclip
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm noimpact
}

// use this near the trigger hurts, lava, death fogs, etc.
// to keep weapons and powerups from piling up...
textures/common/nodrop
{
	surfaceparm nodrop
	surfaceparm nomarks
	surfaceparm nolightmap
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans

	cull none
}

textures/common/trigger
{
	surfaceparm nodraw
}

textures/common/origin
{
	surfaceparm origin
	surfaceparm nodraw
	surfaceparm nonsolid
}

// aids in VIS compiles
textures/common/hint
{
	surfaceparm hint
	surfaceparm structural
	surfaceparm nodraw
	surfaceparm noimpact
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common/skip
{
	surfaceparm skip
	surfaceparm structural
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common/nodraw
{
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm nomarks
	surfaceparm trans
}

// for an icy effect
textures/common/slick
{
	surfaceparm slick
	surfaceparm nodraw
	surfaceparm nomarks
	surfaceparm trans
}

// to keep certain textures from being shot up
textures/common/weapclip
{
	surfaceparm nodraw
	surfaceparm nomarks
	surfaceparm trans
}

// hint for the bots
textures/common/clusterportal
{
	surfaceparm clusterportal
	surfaceparm nodraw
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans

	qer_nocarve
}

// nicked from nexuiz
textures/common/metalclip
{
	surfaceparm playerclip
	surfaceparm metalsteps
	surfaceparm nodraw
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nonsolid
	surfaceparm trans
}

// acts as player clip only for the bots
// can keep them from being pushed into voids
// do not use, use donotenter instead :-P
textures/common/botclip
{
	surfaceparm botclip
	surfaceparm nodraw
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nonsolid
	surfaceparm trans
}

// for the idiot bots out there
textures/common/donotenter
{
	surfaceparm donotenter
	surfaceparm nodraw
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common/missileclip
{
	surfaceparm playerclip
	surfaceparm nodamage
	surfaceparm nomarks
	surfaceparm nodraw
	surfaceparm trans
}

textures/common/areaportal
{
	surfaceparm areaportal
	surfaceparm structural
	surfaceparm nodraw
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common/antiportal
{
	surfaceparm antiportal
	surfaceparm structural
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans

	qer_nocarve
}

// andrewj: added this.  The main problem with making a ladder from clips is
//          the bots try to walk down it like a staircase (very slowly).
//          AJBOT3 detects this brush and inhibits those reachabilities.
textures/common/ladderclip
{
	surfaceparm playerclip
	surfaceparm ladder
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm noimpact
}
