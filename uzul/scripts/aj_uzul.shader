//
// Uzul shaders
//

textures/aj_uzul/sky
{
	skyparms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	qer_editorimage textures/aj_uzul/sky1.tga

	q3map_lightRGB 1 1 1
	q3map_skylight 100 16
	q3map_sunExt 1 0.8 .6  200 30 50  4 25

	{
		map textures/aj_uzul/sky1
		tcmod scale 2 3
		tcmod scroll 0.03 0.03
	}
	{
		map textures/aj_uzul/sky2
		blendfunc add
		tcmod scale 3 2
		tcmod scroll 0.015 0.015
	}
}

textures/aj_uzul/behindtele
{
	surfaceparm noimpact
	surfaceparm nomarks

	qer_editorimage textures/aj_uzul/rock.jpg
	{
		map textures/aj_uzul/rock
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_uzul/telefx
{
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm noimpact
	surfaceparm nomarks

	tessSize 32

	qer_editorimage textures/aj_uzul/telefx.tga
	{
		map textures/aj_uzul/teleblack
		blendFunc blend
	}
	{
		map textures/aj_uzul/telefx
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
		tcMod turb 3 .15 3 .3
	}
}

textures/aj_uzul/water
{
	cull none

//	surfaceparm water
	surfaceparm trans
	surfaceparm nonsolid

	deformVertexes wave 64 sin .5 .5 0 .5

	q3map_globaltexture

	qer_editorimage textures/aj_uzul/pool3.jpg
	qer_trans .5
	{
		map textures/aj_uzul/pool3
		rgbGen identity
		tcMod scale .5 .5
		tcMod transform 1.5 0 1.5 1 1 2
		tcMod scroll -.05 .001
		blendFunc GL_DST_COLOR GL_ONE
	}
	{
		map textures/aj_uzul/pool3
		rgbGen identity
		tcMod scale .5 .5
		tcMod transform 0 1.5 1 1.5 2 1
		tcMod scroll .025 -.001
		blendFunc GL_DST_COLOR GL_ONE
	}
	{
		map textures/aj_uzul/pool3
		rgbGen identity
		tcMod scale .25 .5
		tcMod scroll .001 .025
		blendFunc GL_DST_COLOR GL_ONE
	}
}

textures/aj_uzul/flame
{
	cull disable

	surfaceparm trans
	surfaceparm nolightmap
	surfaceparm nomarks

	qer_editorimage textures/aj_uzul/flame1.tga
	{
		animMap 8 textures/aj_uzul/flame1.tga textures/aj_uzul/flame2.tga textures/aj_uzul/flame3.tga textures/aj_uzul/flame4.tga textures/aj_uzul/flame5.tga textures/aj_uzul/flame6.tga textures/aj_uzul/flame7.tga textures/aj_uzul/flame8.tga
		rgbGen identity
		blendFunc blend
	}
}

textures/aj_uzul/torchwood
{
	cull disable

	surfaceparm nonsolid
	surfaceparm trans
	surfaceparm nomarks

	qer_editorimage textures/aj_uzul/torchwood.jpg

	{
		map textures/aj_uzul/torchwood
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_uzul/jumppad
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodlight

	tessSize 16

	q3map_lightRGB 1 0.6 0.2
	q3map_surfacelight 2200

	qer_editorimage textures/aj_uzul/jumppad.jpg
	{
		map textures/aj_uzul/jumppad
		rgbGen const ( 0.5 0.5 0.5 )
		tcMod rotate 12
		tcMod turb 0 0.1 0 0.2
	}
}
