
AJDM
====

by Andrew Apted, 2022.


About
-----

AJDM is a set of maps for OpenArena and Quake 3 Arena.  The goal was
to collect a group of free DeathMatch maps which have good gameplay,
retexture them to use 100% free textures, and ensure they conform to
certain principles on how they look and play.  For example, how well
you are able to see the other players is a prime concern.

These maps were all created by other people, who generously released
their map sources under open source licenses, such as the GNU General
Public License (GPL).  The textures are mostly from OpenArena, but a
few of them have been sourced from OpenGameArt.  The egyptian texture
set is by Simon O'Callaghan, who gave permission for those textures
to be distributed under the terms of the GNU GPL version 2.

As the name implies, the focus of these maps is purely DeathMatch mode
(also called `FFA` or Free-for-all), which includes Team DeathMatch and
Tournament (two-player duels).  I'm not going to support more exotic
game modes in these maps (such as domination, kill the obelisk, etc)
since I really don't care about those game modes.


The Maps
--------

AJDM contains 26 maps, one for each letter of the English alphabet.
During development, I had about 19 or 20 maps and noticed that most
of their short names began with a different letter, so I decided it
would be cool to have the full set from A to Z.  This meant coming
up with new names for a few of the maps, to prevent clashing with an
existing name, though I admit some of my names are not that great.

....


Principles
----------

## Gameplay

Two-way teleporters are avoided as much as possible.  It is very
annoying to be killed while trying to enter a teleporter.  Another
issue is that bots often behave stupidly with two-way teleporters,
going through a teleporter and back again several times in a row.
Only two maps in AJDM contain such teleporters: `five` and `nemesis`.
They were removed from a few maps, notably: `aggressor`.

Maps which are perfectly symmetrical (as a whole) are quite boring,
both to look at and to play.  I rejected several maps from the OACMP
sets, as well as the map `ce1m7`, from inclusion into AJDM because
they were too symmetrical.  The only perfectly symmetrical map in
AJDM is `yard`, though `schism` also suffers from having a bit too
much symmetry.

Most of the powerups are avoided, such as invisibility.  I find them
really gimmicky, and just make for gameplay worse.  One map `nemesis`
contains the haste item (it is such a large map that it makes sense
there).  Only seven maps contain the quad damage item.  None of the
maps in AJDM contain the BFG weapon.

## Textures

It is very important to be able to see the other players in the map.
My initial goal with AJDM was to produce maps with a "clean" aesthetic,
to use fairly plain textures to make it easier to see other players.

....

## Sounds

It is also important to be able to hear the other players in the map.
Hence none of the maps in AJDM have any environmental sounds, except
for some bird sounds in `pump`.  Only one map, `cyclone`, has floors
which play the metallic footstep sounds.  Furthermore, there is no
music for any of these maps.

## Lighting

8x8 lightmap resolution

....


Compiling
---------

This section describes how to compile the maps from their source.

- AJMAP3
- AJBOT3
- GNU make

....


Legal Stuff
-----------

## Maps

The maps `beer`, `pump`, `schism` and `war` are under the GNU
General Public License (GPL) version 3.

See: https://www.gnu.org/licenses/gpl-3.0.html

The remaining maps are under the GNU General Public License (GPL)
version 2.

See: https://www.gnu.org/licenses/gpl-2.0.html

## Textures

I personally made a few textures from scratch, including the blue
circuit-board one, and I release these under the CC0 license
(i.e. as public domain).

Some textures from OpenGameArt are also under the CC0 license: door
images by user "para", classical paintings by user "eleazzaar", crate
textures by user "H-Hour", and a textile image by user "rubberduck".

Other textures from OpenGameArt are under the Creative Commons with
Attribution (CC-BY) license: a tiled-floor texture and a brick wall
texture by user "Spiney", a skybox by Jockum Skoglund, and a carpet
texture by user "Keith333".

See: https://creativecommons.org/licenses/by/3.0/

The egyptian texture set is by Simon O'Callaghan, who gave permission
for those textures to be distributed under the terms of the GNU General
Public License (GPL) version 2.

The remaining textures are from OpenArena, and hence are under the
GNU General Public License (GPL) version 2.

