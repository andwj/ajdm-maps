#
#  Makefile for AJDM Maps
#
#  This will create the PK3 for every map in AJDM.
#  Each map is compiled to a BSP file, then lit and vis'd,
#  and its AAS file for bot support is constructed.
#
#  NOTE: requires GNU make and sh/bash/dash shell
#

MAPS=	aggressor beer cyclone dungeon elbow five \
	genius healer irony junction keep lilith  \
	manor nemesis osiris pump quadrun ritual  \
	schism trial uzul vertigo war xray yard zed

all: $(MAPS)

$(MAPS) :
	@echo Building $@....
	@cd $@ && $(MAKE) --silent
	@echo
	@echo ==========================================
	@echo

clean:
	@for dir in $(MAPS); do echo Cleaning $$dir....; cd $$dir; $(MAKE) --silent clean; cd ..; done

.PHONY: all clean $(MAPS)

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
