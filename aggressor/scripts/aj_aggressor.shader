//
// Aggressor shaders
//

textures/aj_aggressor/sky
{
	skyParms - 512 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_skylight 300 16
	q3map_globaltexture

	qer_editorimage textures/aj_aggressor/sky1.jpg
	{
		map textures/aj_aggressor/sky1
		tcMod scroll 0.026 0.036
//		tcMod scale 3 2
		depthWrite
	}
	{
		map textures/aj_aggressor/sky1
		blendfunc add
		tcMod scroll 0.02 0.02
		tcMod scale 2 2
	}
}

textures/aj_aggressor/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 0.3 0.7 1
	q3map_surfacelight 900

	qer_editorimage textures/aj_aggressor/tele1.jpg
	{
		map textures/aj_aggressor/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_aggressor/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_aggressor/tlamp
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 4000
	qer_editorimage textures/aj_aggressor/tlamp.tga

	{
		map textures/aj_aggressor/tlamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_aggressor/tlamp_glow
		blendFunc add
	}
}

textures/aj_aggressor/tlamp6
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 15000
	qer_editorimage textures/aj_aggressor/tlamp.tga

	{
		map textures/aj_aggressor/tlamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_aggressor/tlamp_glow
		blendFunc add
	}
}

textures/aj_aggressor/drkmtl_light
{
	q3map_surfacelight 4000
	q3map_lightRGB 1.00 0.81 0.61
	qer_editorimage textures/aj_aggressor/drkmtl_light.tga

	{
		map textures/aj_aggressor/drkmtl_light
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_aggressor/drkmtl_light_glow
		blendFunc add
		rgbGen const ( 0.7 0.7 0.7 )
	}
}
