#
#  Common rules for building each map.
#
#  NOTE: requires GNU make
#

# this is relative to the current map's directory
MAIN_DIR=..

SHARED_DIR=$(MAIN_DIR)/_shared

# find the tools in the "_shared" directory
AJMAP3=$(SHARED_DIR)/ajmap3
AJBOT3=$(SHARED_DIR)/ajbot3

PK3_FILE=ajdm-map-$(NAME).pk3
SOURCE_ZIP=ajdm-source-$(NAME).zip

all: $(PK3_FILE)

clean:
	rm -f $(PK3_FILE) $(SOURCE_ZIP)
	rm -f $(NAME).bsp $(NAME).aas $(NAME).log
	rm -f $(NAME).srf $(NAME).prt $(NAME).lin
	rm -f $(NAME).bak $(NAME).autosave.map
	rm -f maps/*.*

full: clean all

$(PK3_FILE): $(NAME).bsp $(NAME).aas
	@rm -f $@
	@mkdir -p maps
	@cp $(NAME).bsp  maps/$(NAME).bsp
	@cp $(NAME).aas  maps/$(NAME).aas
	@cp AUTHORS.txt maps/$(NAME)_AUTHORS.txt
	@cp LICENSE.txt maps/$(NAME)_LICENSE.txt
	zip -q -r $@ maps scripts levelshots env textures
	zip -q -l $@ maps/*.txt

$(NAME).bsp: $(NAME).map
	@rm -f $@
	$(AJMAP3) -bsp   $^ -path $(SHARED_DIR)
	$(AJMAP3) -light $^ -path $(SHARED_DIR)
	$(AJMAP3) -vis   $^ -path $(SHARED_DIR)

$(NAME).aas: $(NAME).bsp
	@rm -f $@
	$(AJBOT3) -bsp2aas $^

# 'unlit' target produces the PK3, but with no lighting / vis / aas
unlit:
	@rm -f $(NAME).bsp
	$(AJMAP3) -bsp $(NAME).map -path $(SHARED_DIR)
	touch $(NAME).aas
	$(MAKE) $(PK3_FILE)

# 'lit' target produces the PK3, but with no vis / aas
lit:
	@rm -f $(NAME).bsp
	$(AJMAP3) -bsp   $(NAME).map -path $(SHARED_DIR)
	$(AJMAP3) -light $(NAME).map -path $(SHARED_DIR)
	touch $(NAME).aas
	$(MAKE) $(PK3_FILE)

# 'bot' target produces the PK3, but with no vis / light
bot:
	@rm -f $(NAME).bsp
	$(AJMAP3) -bsp   $(NAME).map -path $(SHARED_DIR)
	$(MAKE) $(NAME).aas
	$(MAKE) $(PK3_FILE)

# 'source' target creates a zip with the map sources
source:
	@rm -f $(SOURCE_ZIP)
	zip -q -l $(SOURCE_ZIP) $(NAME).map $(NAME).cfg
	zip -q -l $(SOURCE_ZIP) AUTHORS.txt LICENSE.txt

.PHONY: all clean full unlit lit bot

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
