//
// Genius shaders
//

textures/aj_genius/sky
{
	skyParms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 100 9
	q3map_globaltexture

	qer_editorimage textures/aj_genius/sky1.jpg
	{
		map textures/aj_genius/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_genius/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/aj_genius/jumppad
{
	surfaceparm nomarks

	qer_editorimage textures/aj_genius/jumppad.jpg
	{
		map textures/aj_genius/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_genius/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.6 0.4 0 1.0
	}
}

textures/aj_genius/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodlight

	q3map_tessSize 32

	qer_editorimage textures/aj_genius/telefx.jpg
	{
		map textures/aj_genius/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll -0.03 -0.06
		tcMod turb 0 1 0 0.04
	}
	{
		map textures/aj_genius/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll 0.07 0.04
		tcMod turb 0 4 0 0.02
		blendFunc add
	}
	{
		map textures/aj_genius/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll -0.05 0.03
		tcMod turb 0 3 0 0.03
		blendFunc add
	}
}

textures/aj_genius/darkgrate
{
	surfaceparm trans

	qer_editorimage textures/aj_genius/darkgrate.tga
	{
		map textures/aj_genius/darkgrate.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_genius/proto_grate
{
	cull disable
	surfaceparm trans

	qer_editorimage textures/aj_genius/proto_grate.tga
	{
		map textures/aj_genius/proto_grate.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_genius/sqlight4
{
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_surfacelight 3700

	qer_editorimage textures/aj_genius/light3.tga
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_genius/light3.tga
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
	{
		map textures/aj_genius/light3_glow.tga
		blendfunc add
	}
}

textures/aj_genius/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 500

	qer_editorimage textures/aj_genius/fluoro4.jpg
	{
		map textures/aj_genius/fluoro4.jpg
		rgbGen const ( 0.6 0.6 0.6 )
	}
//	{
//		map textures/aj_genius/fluoro4_glow.jpg
//		blendFunc add
//	}
}

textures/aj_genius/fluoro6
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 400

	qer_editorimage textures/aj_genius/fluoro4.jpg
	{
		map textures/aj_genius/fluoro4.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
//	{
//		map textures/aj_genius/fluoro4_glow.jpg
//		blendFunc add
//	}
}

textures/aj_genius/fluorowide
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 600

	qer_editorimage textures/aj_genius/fluoro1.jpg
	{
		map textures/aj_genius/fluoro1.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
//	{
//		map textures/aj_genius/fluoro1_glow.jpg
//		blendFunc add
//	}
}

textures/aj_genius/doorstripe
{
	surfaceparm nomarks

	qer_editorimage textures/aj_genius/doorstripe.jpg
	{
		map textures/aj_genius/doorstripe.jpg
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_genius/doorstripe_glow.jpg
		rgbGen wave sin 0.4 0.4 0 0.5
		blendFunc add
	}
}

textures/aj_genius/framestripe
{
	surfaceparm nomarks

	qer_editorimage textures/aj_genius/doorstripe.jpg
	{
		map textures/aj_genius/doorstripe.jpg
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_genius/doorstripe_glow.jpg
		rgbGen wave sin 0.4 0.4 0 0.5
		blendFunc add
	}
}

textures/aj_genius/xlight5
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 700

	qer_editorimage textures/aj_genius/xlight5.tga
	{
		map textures/aj_genius/xlight5.tga
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_genius/grass
{
	// this prevents radiosity making nearby stuff green
	q3map_lightImage textures/aj_genius/wall1.jpg

	qer_editorimage textures/aj_genius/grass.jpg
	{
		map textures/aj_genius/grass.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}
