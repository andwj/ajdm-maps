//
// Xray shaders
//

textures/aj_xray/sky
{
	skyParms env/aj_xray/miramar - -

	qer_editorimage env/aj_xray/miramar_up.tga

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_skylight 55 16
	q3map_sun 1.00 0.90 0.80 25 327 57
	q3map_globaltexture

	// andrewj: pure skybox, no cloud stages here
	//          [ clouds won't draw the bottom face ]
}

textures/aj_xray/flylamp
{
	surfaceparm nomarks
	surfaceparm nolightmap

	qer_editorimage textures/aj_xray/flylamp.tga

	{
		map textures/aj_xray/flylamp
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_xray/flylamp_glow
		rgbGen wave sin 0.3 0.3 0 0.7
		blendFunc add
	}
}

textures/aj_xray/b_lamp1
{
	surfaceparm nomarks
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 1400

	qer_editorimage textures/aj_xray/b_lamp.tga
	{
		map textures/aj_xray/b_lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_xray/b_lamp2
{
	surfaceparm nomarks
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 500

	qer_editorimage textures/aj_xray/b_lamp.tga
	{
		map textures/aj_xray/b_lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_xray/b_lamp3
{
	surfaceparm nomarks
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 1600

	qer_editorimage textures/aj_xray/b_lamp.tga
	{
		map textures/aj_xray/b_lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_xray/tlite
{
	qer_editorimage textures/aj_xray/tlite.tga

	{
		map textures/aj_xray/tlite
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_xray/tlite_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_xray/trim2
{
	surfaceparm nomarks

	qer_editorimage textures/aj_xray/trim2.tga

	{
		map textures/aj_xray/trim2
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_xray/trim2_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_xray/jumper
{
	surfaceparm nomarks
	surfaceparm nolightmap

	qer_editorimage textures/aj_xray/jumper.tga

	{
		map textures/aj_xray/jumper
		rgbGen const ( 0.23 0.23 0.23 )
	}
	{
		map textures/aj_xray/jumper_glow
		rgbGen wave sin 0.2 0.2 0 1
		blendFunc add
	}
}

textures/aj_xray/banner
{
	surfaceparm nomarks
	surfaceparm nodlight
	surfaceparm nolightmap

	qer_editorimage textures/aj_xray/banner.tga
	{
		map textures/aj_xray/banner
		rgbGen wave square 0.8 0.2 0 0.5
		tcMod scroll 0.3 0
	}
//	{
//		map textures/aj_xray/banner_frame
//		rgbGen const ( 0.4 0.4 0.4)
//		blendFunc blend
//	}
}
