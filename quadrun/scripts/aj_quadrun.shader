//
// Quadrun shaders
//

textures/aj_quadrun/sky
{
	skyParms full 600 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_sun 1.00 0.80 0.60 200 300 58
	q3map_lightRGB 0.9 0.9 1.0
	q3map_skyLight 70 16
	q3map_globaltexture

	qer_editorimage textures/aj_quadrun/sky1.tga
	{
		map textures/aj_quadrun/sky1
		tcmod scale 2 3
		tcmod scroll 0.03 0.03
	}
	{
		map textures/aj_quadrun/sky2
		blendfunc add
		tcmod scale 3 2
		tcmod scroll 0.015 0.015
	}
}

textures/aj_quadrun/water
{
	surfaceparm water
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nomarks

	qer_editorimage textures/aj_quadrun/pool5e.jpg
	qer_trans 0.5

	{
		map textures/aj_quadrun/pool5e.jpg
		rgbGen const ( 0.7 0.7 0.7 )
		tcmod turb .04 .01 .5 .03
		blendFunc GL_DST_COLOR GL_ONE
	}
	{
		map textures/aj_quadrun/pool6.jpg
		rgbGen const ( 0.7 0.7 0.7 )
		tcMod scroll .025 -.001
		blendFunc GL_DST_COLOR GL_ONE
	}
	{
		map textures/aj_quadrun/pool5.jpg
		rgbGen const ( 0.7 0.7 0.7 )
		tcMod scroll .001 .025
		blendFunc GL_DST_COLOR GL_ONE
	}
}

textures/aj_quadrun/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nodlight
	surfaceparm nolightmap

	tesssize 64

	q3map_surfacelight 20

	qer_editorimage textures/aj_quadrun/tele1.jpg
	{
		map textures/aj_quadrun/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_quadrun/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_quadrun/jumppad
{
	surfaceparm nomarks

	q3map_lightRGB 1 1 0.3
	q3map_surfacelight 300

	qer_editorimage textures/aj_quadrun/jumppad.jpg
	{
		map textures/aj_quadrun/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_quadrun/jumppad_glow.tga
		rgbGen wave sin 0.6 0.4 0 1.0
		blendFunc add
	}
}

textures/aj_quadrun/weaponspot
{
	qer_editorimage textures/aj_quadrun/cross1.tga
	{
		map textures/aj_quadrun/cross1.tga
		rgbGen const ( 0.85 0.85 0.85 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_quadrun/cross1_glow.tga
		rgbGen wave sin 0.5 0.3 0 0.35
		blendFunc add
	}
}

textures/aj_quadrun/lamp1
{
	q3map_lightRGB 0.9 0.9 1.0
	q3map_surfacelight 2000

	qer_editorimage textures/aj_quadrun/lamp.jpg

	{
		map textures/aj_quadrun/lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_quadrun/lamp_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_quadrun/lamp2
{
	q3map_lightRGB 0.9 0.9 1.0
	q3map_surfacelight 4000

	qer_editorimage textures/aj_quadrun/lamp.jpg

	{
		map textures/aj_quadrun/lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_quadrun/lamp_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_quadrun/lamp3
{
	q3map_lightRGB 0.9 0.9 1.0
	q3map_surfacelight 7000

	qer_editorimage textures/aj_quadrun/lamp.jpg

	{
		map textures/aj_quadrun/lamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_quadrun/lamp_glow
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}

textures/aj_quadrun/wall
{
	// this prevents radiosity making nearby stuff pink
	q3map_lightImage textures/aj_quadrun/arch1.jpg

	qer_editorimage textures/aj_quadrun/wall.jpg
	{
		map textures/aj_quadrun/wall.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}
