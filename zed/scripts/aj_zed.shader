//
// Zed shaders
//

textures/aj_zed/sky
{
	skyParms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 250 16
	q3map_globaltexture

	qer_editorimage textures/aj_zed/sky1.jpg
	{
		map textures/aj_zed/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_zed/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/aj_zed/teleporter
{
	surfaceparm nomarks
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 1.0 0.9 0.7
	q3map_surfacelight 200

	qer_editorimage textures/aj_zed/telefx.jpg
	{
		map textures/aj_zed/telefx
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_zed/telefx
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_zed/jumppad
{
	surfaceparm nomarks

//	q3map_lightRGB 1.0 0.7 .3
//	q3map_surfacelight 100

	qer_editorimage textures/aj_zed/jumppad.jpg
	{
		map textures/aj_zed/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_zed/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.6 0.4 0 1.0
	}
}

textures/aj_zed/tlamp2
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_zed/tlamp.tga

	{
		map textures/aj_zed/tlamp.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_zed/tlamp_glow.tga
		blendFunc add
	}
}

textures/aj_zed/tlamp3
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 6000
	qer_editorimage textures/aj_zed/tlamp.tga

	{
		map textures/aj_zed/tlamp.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_zed/tlamp_glow.tga
		blendFunc add
	}
}

textures/aj_zed/fluoro1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000
	qer_editorimage textures/aj_zed/fluoro.jpg
	{
		map textures/aj_zed/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_zed/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_zed/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 2500
	qer_editorimage textures/aj_zed/fluoro.jpg
	{
		map textures/aj_zed/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_zed/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_zed/fluoroside
{
	qer_editorimage textures/aj_zed/fluoroside.jpg
	{
		map textures/aj_zed/fluoroside.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_zed/ceil1
{
	qer_editorimage textures/aj_zed/floor.jpg

	{
		map textures/aj_zed/floor.jpg
		rgbGen identity  // mimick engine behavior
	}
	{
		map $lightmap
		blendFunc filter
	}
}
