//
// War shaders
//

textures/aj_war/sky
{
	skyParms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_sun 1.00 0.80 0.60 150 305 33
	q3map_lightRGB 1 1 1
	q3map_skylight 80 9
	q3map_globaltexture

	qer_editorimage textures/aj_war/sky1.jpg
	{
		map textures/aj_war/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_war/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/aj_war/teleporter
{
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_surfaceLight 200
	q3map_lightSubdivide 64

	qer_editorimage textures/aj_war/tesla1b.jpg
	{
		map textures/aj_war/tesla1b.jpg
		rgbGen identity
		tcMod scroll -8.31 1.21
	}
	qer_editorimage textures/aj_war/tesla1b.jpg
	{
		map textures/aj_war/tesla1b.jpg
		rgbGen identity
		tcMod scroll -12.73 1.76
		blendFunc add
	}
	qer_editorimage textures/aj_war/tesla1b.jpg
	{
		map textures/aj_war/tesla1b.jpg
		rgbGen identity
		tcMod scroll -19.26 0.95
		blendFunc add
	}
}

textures/aj_war/jumppad
{
	surfaceparm nodamage
	polygonoffset

	{
		map textures/aj_war/jumppad.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_war/jumppad_glow.tga
		tcMod stretch sine 1.2 0.2 0 0.77
		rgbGen const ( 0.4 0.4 0.4 )
		blendFunc add
	}
	{
		map textures/aj_war/jumppad_outer.tga
		rgbGen const ( 0.5 0.5 0.5 )
		alphaFunc GE128
	}
}

textures/aj_war/lamp1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_war/lamp.tga
	{
		map textures/aj_war/lamp.tga
	}
	{
		map textures/aj_war/lamp_glow.tga
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_war/lamp2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 6000
	qer_editorimage textures/aj_war/lamp.tga
	{
		map textures/aj_war/lamp.tga
	}
	{
		map textures/aj_war/lamp_glow.tga
		rgbGen const ( 0.25 0.25 0.25 )
		blendFunc add
	}
}

textures/aj_war/lamp3
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 9000
	qer_editorimage textures/aj_war/lamp.tga
	{
		map textures/aj_war/lamp.tga
	}
	{
		map textures/aj_war/lamp_glow.tga
		rgbGen const ( 0.25 0.25 0.25 )
		blendFunc add
	}
}
