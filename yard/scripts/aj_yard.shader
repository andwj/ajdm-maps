//
// Yard shaders
//

textures/aj_yard/sky
{
	skyParms env/aj_yard/starry 128 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_sunExt 1 1 1 26  0 90  5 64

	qer_editorimage env/aj_yard/starry_rt.tga

	// pure skybox, no cloud stages
}

textures/aj_yard/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodlight

	qer_editorimage textures/aj_yard/telefx.jpg
	{
		map textures/aj_yard/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll -0.03 -0.06
		tcMod turb 0 1 0 0.04
	}
	{
		map textures/aj_yard/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll 0.07 0.04
		tcMod turb 0 4 0 0.02
		blendFunc add
	}
	{
		map textures/aj_yard/telefx.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod scroll -0.05 0.03
		tcMod turb 0 3 0 0.03
		blendFunc add
	}
}

textures/aj_yard/jumppad
{
	surfaceparm nodamage
	polygonoffset

	{
		map textures/aj_yard/jumppad.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_yard/jumppad_glow.jpg
		tcMod scroll 0 1.7
		blendFunc add
	}
	{
		map textures/aj_yard/jumppad.tga
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc blend
	}
}

textures/aj_yard/fluoro
{
	qer_editorimage textures/aj_yard/fluoro1.jpg
	{
		map textures/aj_yard/fluoro1.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_yard/fluoro1_glow.jpg
		blendFunc add
	}
}

textures/aj_yard/tlite_3k
{
	q3map_surfacelight 1732
	q3map_lightRGB 1 1 1

	qer_editorimage textures/aj_yard/tlite.tga
	{
		map textures/aj_yard/tlite.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_yard/tlite_glow.jpg
		blendFunc add
	}
}
