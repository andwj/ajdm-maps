//
// Pump shaders
//

textures/aj_pump/sky
{
	skyParms env/aj_pump/miramar - -

	qer_editorimage env/aj_pump/miramar_up.tga

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_globaltexture
	q3map_sun  1.0 0.6 0.4  152  155  57
	q3map_lightRGB 1 0.9 0.82
	q3map_skylight 150 9

	// andrewj: pure skybox, no cloud stages here
}

textures/aj_pump/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	q3map_lightRGB 0.2 0.6 1.0
	q3map_surfacelight 150
	tesssize 64

	qer_editorimage textures/aj_pump/telefx.jpg
	{
		map textures/aj_pump/telefx.jpg
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_pump/telefx.jpg
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc add
	}
}

textures/aj_pump/jumppad
{
	surfaceparm nomarks

//	q3map_surfacelight 150
//	q3map_lightRGB 1 0.2 0.2

	qer_editorimage textures/aj_pump/jumppad.jpg
	{
		map textures/aj_pump/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_pump/jumppad_glow.tga
		rgbGen wave sin 0.5 0.5 0 1.0
		blendFunc add
	}
}

textures/aj_pump/crow1a
{
	cull disable
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nomarks
	qer_editorimage textures/aj_pump/bird3b.tga
	{
		map textures/aj_pump/bird3b.tga
		rgbGen identity
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		rgbGen identity
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_pump/crow2a
{
	cull disable
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nomarks
	deformVertexes move 1 0.3 0.5 sin 0 72 0 0.08
	qer_editorimage textures/aj_pump/bird3b.tga
	{
		map textures/aj_pump/bird3b.tga
		rgbGen identity
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		rgbGen identity
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_pump/tlamp1
{
	q3map_lightRGB 1 0.6 .2
	q3map_surfacelight 2000

	qer_editorimage textures/aj_pump/tlamp1.tga

	{
		map textures/aj_pump/tlamp1
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_pump/tlamp2
{
	q3map_lightRGB 1 0.6 .2
	q3map_surfacelight 6000

	qer_editorimage textures/aj_pump/tlamp1.tga

	{
		map textures/aj_pump/tlamp1
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_pump/fluoro1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 800

	qer_editorimage textures/aj_pump/fluoro1.jpg

	{
		map textures/aj_pump/fluoro1
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_pump/fluoro4
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 5100

	qer_editorimage textures/aj_pump/fluoro4.jpg

	{
		map textures/aj_pump/fluoro4
		rgbGen const ( 0.5 0.5 0.5 )
	}
}
