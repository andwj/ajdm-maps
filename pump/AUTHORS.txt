
Pump AUTHORS
============

The original map was created by Lunaran (Matthew Breit).
Later it was released under the GNU GPL license (v3).

It has been modified by me (Andrew Apted) for AJDM, and I take
full responsibility for how it currently looks and plays.

Below is the document included with "lun3dm4src.zip", and
further below that is the original text file.


--->

- Lun3DM4src: File Information ------------------------------------------- 03.05.2007 ---

Filename: Lun3DM4src.zip
Author: Matt "Lunaran"  Breit
Email: matt@lunaran.com
Web Site: http://www.lunaran.com
Description: Source .map files for Lun3DM4: Pull Your Socks Up, originally
released in 2003.

No guarantees that your compile will match mine (especially lighting), and no
guarantees that there won't be broken or missing textures.  You're here for the
brushwork and entity placement.

- Copyright / Permissions ----------------------------------------------------------------
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


--->

- LUN3DM4: File Information ----------------------------------------------- 04.10.2003 ---
               Title : Pull Your Socks Up
            Filename : lun3dm4.pk3
              Author : Matt "Lunaran"  Breit
       Email Address : matt@lunaran.com
                       I will not help you with your mod.
           Web Sites : http://www.lunaran.com
                       http://lunaran.fov120.com
   Previous Releases : Lun3DM3 - A Load of Useless Bloody Loonies, Q3DM/CPMA
                       Lun3DM2 - Let's Drink Beer and Shoot Things, Q3DM
                       Lun3DM1 - Coriolis Storm, Q3DM
                       LunQ2DM1 - The Gridlock, Q2DM
                       westfront - Westfront Facility, HLDM
                       complex - The Complex, Q2DM
        Installation : To install, simply unzip and place lun3dm4.pk3 in your baseq3
                       directory wherever you installed Quake3:Arena.  The map should now
                       be accessible via the Multiplayer menu or by typing "\map lun3dm4" at
                       the console without the quotes.
    Acknowledgements : WViperW for tons of important changes from the early alphas
                     : Anyone at the promode.org forums that beat on the alpha/beta for me
                       (thanks guys - enjoy it :) )
                     : RPG, Dietz, Bal, and Scampie for valuable beta feedback.

- Play Information -----------------------------------------------------------------------
         Player Base : Designed for Tournament, but works with up to 5.
           Item Load : 2 SGs, 2 RLs, LG, GL, GA, YA, RA, MH.  The classic load.
  CPMA Version Notes : There are no particular notes on the CPMA version because the map IS
                       the CPMA version.  Lun3DM4 was built, designed, and balanced for CPMA
                       physics and gameplay.  I had no motivation to try this map in VQ3, but
                       it must lack quite a lot of substance - you have none of the trick jumps
                       and none of the speed.
                     : The map should load and play in Quake3 without the green armor breaking
                       the game.

- Construction ---------------------------------------------------------------------------
            Textures : Some stuff left over from Coriolis Force.  May she rest in pieces.
             Brushes : 1768
    Average R_Speeds : 7000 world, w/ entities, no players
 Worst Case R_Speeds : 10000 world, w/ entities, no players
      Editor(s) used : GTKRadiant 1.211, Adobe Photoshop 7
               Birds : 3

- Copyright / Permissions ----------------------------------------------------------------

 * You MAY:
- Use the included custom textures & shaders, or modifications thereof, provided you give
note of such in an attached readme (and please let me know).  Also be sure to use directories
and filenames different than those in lun3dm4.pk3.
- Distribute this pak file and/or its contents by any ELECTRONIC means, provided you leave
the contents unaltered and include this text file, also unaltered.

 * You MAY NOT:
- Decompile or reproduce the BSP as a base to build additional levels.  Make your own damned
maps.
- Commercially exploit this file or its contents in any way.
- Distribute this pak file and/or its contents on any HARD MEDIA whatsoever, including but not
limited to magazine coverdisks or level compilations, without prior consent of and negotiation
with the author.  Not that Q3 maps appear on coverdisks anymore ...



                       ,==================================,
                       |  "Avi!  Pull your socks up!"     |
                       |                                  |
                       |             - Bullet-Tooth Tony  |
                       |                "Snatch"          |
                       '=================================='
