//
// Dungeon shaders
//

textures/aj_dungeon/sky
{
	skyParms full 512 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 0.75 0.70
	q3map_skylight 95 16

	qer_editorimage textures/aj_dungeon/sky1.jpg

	{
		map textures/aj_dungeon/sky1.jpg
		tcMod scale 3 2
		tcMod scroll 0.15 0.15
	}
	{
		map textures/aj_dungeon/sky2.jpg
		tcMod scale 3 3
		tcMod scroll 0.05 0.05
		blendFunc filter
	}
}

textures/aj_dungeon/teleporter
{
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	nopicmip

//	q3map_lightRGB 0.6 0.8 1
//	q3map_surfacelight 100

	qer_editorimage textures/aj_dungeon/teleport.jpg
	{
		map textures/aj_dungeon/teleport.jpg
		rgbGen identity
		tcMod scroll 0 0.2001
	}
	{
		map textures/aj_dungeon/teleport.jpg
		rgbGen identity
		tcMod scroll -0.02 0.2828
		blendFunc add
	}
	{
		map textures/aj_dungeon/teleport.jpg
		rgbGen identity
		tcMod scroll 0.02 0.3465
		blendFunc add
	}
}

textures/aj_dungeon/lamp1
{
	qer_editorimage textures/aj_dungeon/lamp8.jpg

	q3map_lightRGB 1 1 1
	q3map_surfacelight 600

	{
		map textures/aj_dungeon/lamp8
		rgbGen const ( 0.8 0.8 0.8 )
	}
	{
		map $lightmap 
		blendfunc filter
	}
	{
		map textures/aj_dungeon/lamp8_glow
		rgbGen const ( 0.26 0.26 0.26 )
		blendfunc add
	}
}

textures/aj_dungeon/lamp2
{
	qer_editorimage textures/aj_dungeon/lamp8.jpg

	q3map_lightRGB 1 1 1
	q3map_surfacelight 1200

	{
		map textures/aj_dungeon/lamp8
		rgbGen const ( 0.8 0.8 0.8 )
	}
	{
		map $lightmap 
		blendfunc filter
	}
	{
		map textures/aj_dungeon/lamp8_glow
		rgbGen const ( 0.26 0.26 0.26 )
		blendfunc add
	}
}
