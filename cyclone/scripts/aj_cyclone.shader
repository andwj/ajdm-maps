//
// Cyclone shaders
//

textures/aj_cyclone/sky
{
	skyParms full 700 -

	surfaceparm sky
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_globaltexture
	q3map_sun 1.00 0.9 0.8  40 270 90
	q3map_lightRGB 1 1 1
	q3map_skylight 120 9

	qer_editorimage textures/aj_cyclone/sky1.jpg
	{
		map textures/aj_cyclone/sky1.jpg
		tcMod scale 2 3
		tcMod scroll .03 .01
	}
	{
		map textures/aj_cyclone/sky2.jpg
		blendFunc filter
		tcMod scale 3 2
		tcMod scroll 0.10 0.06
	}
}

textures/aj_cyclone/lava
{
	cull none

	surfaceparm lava
	surfaceparm nodrop
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	deformVertexes wave 1 sin 0.01 0.03 0 0.2 

	q3map_surfacelight 80
	q3map_lightimage textures/aj_cyclone/lavahell2.tga
	qer_editorimage  textures/aj_cyclone/lavahell2.tga
	qer_trans 0.5

	{
		map textures/aj_cyclone/lavahell2.tga
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod turb 0.5 -0.4 0.5 0.1
		blendFunc add
	}
	{
		map textures/aj_cyclone/lavahell2.tga
		rgbGen const ( 0.25 0.25 0.25 )
		tcMod turb 0.4 -0.4 0.7 0.1
		blendFunc add
	}
}

textures/aj_cyclone/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodlight

//	q3map_surfacelight 50

	qer_editorimage textures/aj_cyclone/tele1.jpg
	{
		map textures/aj_cyclone/tele1
		tcMod rotate 142
	}
	{
		map textures/aj_cyclone/tele1
		tcMod rotate 182
		blendFunc add
	}
}

textures/aj_cyclone/walkway
{
	cull none
	surfaceparm trans

	qer_editorimage textures/aj_cyclone/walkway1.tga
	{
		map textures/aj_cyclone/walkway1.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_cyclone/jumppad1
{
	surfaceparm nomarks

//	q3map_surfacelight 150
//	q3map_lightRGB 1 0.2 0.2

	qer_editorimage textures/aj_cyclone/jumppad1.jpg
	{
		map textures/aj_cyclone/jumppad1.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_cyclone/jumppad_glow.tga
		rgbGen wave sin 0.6 0.4 0 1.0
		blendFunc add
	}
}

textures/aj_cyclone/jumppad2
{
	surfaceparm nomarks

//	q3map_surfacelight 150
//	q3map_lightRGB 1 0.2 0.2

	qer_editorimage textures/aj_cyclone/jumppad2.jpg
	{
		map textures/aj_cyclone/jumppad2.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_cyclone/jumppad_glow.tga
		rgbGen wave sin 0.6 0.4 0 1.0
		blendFunc add
	}
}

textures/aj_cyclone/tlamp1
{
	q3map_lightRGB 0.6 0.8 1
	q3map_surfacelight 5000
	qer_editorimage textures/aj_cyclone/tlamp1.tga

	{
		map textures/aj_cyclone/tlamp1.tga
		rgbGen const ( 0.6 0.6 0.6 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_cyclone/tlamp1_glow.tga
		blendFunc add
	}
}

textures/aj_cyclone/tlamp_white
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 4000
	qer_editorimage textures/aj_cyclone/tlamp1.tga

	{
		map textures/aj_cyclone/tlamp1.tga
		rgbGen const ( 0.6 0.6 0.6 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_cyclone/tlamp1_glow.tga
		blendFunc add
	}
}

textures/aj_cyclone/rlamp2
{
	q3map_lightRGB 0.9 0.95 1
	q3map_surfacelight 1200
	qer_editorimage textures/aj_cyclone/rlamp.jpg

	{
		map textures/aj_cyclone/rlamp
		rgbGen const ( 0.8 0.8 0.8 )
	}
	{
		map $lightmap
		blendFunc filter
	}
//	{
//		map textures/aj_cyclone/rlamp_glow.tga
//		rgbGen const ( 0.3 0.3 0.3 )
//		blendFunc add
//	}
}

textures/aj_cyclone/rlamp4
{
	q3map_lightRGB 0.9 0.95 1
	q3map_surfacelight 4000
	qer_editorimage textures/aj_cyclone/rlamp.jpg

	{
		map textures/aj_cyclone/rlamp
		rgbGen const ( 0.8 0.8 0.8 )
	}
	{
		map $lightmap
		blendFunc filter
	}
//	{
//		map textures/aj_cyclone/rlamp_glow.tga
//		rgbGen const ( 0.3 0.3 0.3 )
//		blendFunc add
//	}
}

textures/aj_cyclone/box
{
	surfaceparm nonsolid

	qer_editorimage textures/aj_cyclone/box.jpg

	{
		map textures/aj_cyclone/box.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_cyclone/metalweapclip
{
	surfaceparm nodraw
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm trans
	surfaceparm metalsteps
}

