//
// Lilith shaders
//

textures/aj_lilith/sky
{
	skyParms - 800 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 600 16

	qer_editorimage textures/aj_lilith/sky1.jpg

	{
		map textures/aj_lilith/sky1
		tcMod scale 3 2
		tcMod scroll 0.08 0.08
	}
	{
		map textures/aj_lilith/sky1
		tcMod scale 3 3
		tcMod scroll 0.03 0.03
		blendFunc add
	}
}

textures/aj_lilith/slime
{
	cull none

	surfaceparm slime
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodrop

	fogparms ( 0.247059 0.729412 0.047216 ) 808
	surfaceparm fog

//	q3map_lightimage textures/aj_lilith/slime8.jpg
//	q3map_lightSubdivide 32
//	q3map_surfacelight 100

	qer_editorimage  textures/aj_lilith/slime8.jpg
	{
		map textures/aj_lilith/slime8.jpg
		rgbGen const ( 0.11 0.11 0.11 )
		tcMod turb .05 -0.5 0 0.02
		tcMod scroll .05 -.01
		blendFunc add
	}
}

textures/aj_lilith/ladder
{
	surfaceparm nonsolid

	qer_editorimage textures/aj_lilith/ladder.jpg

	{
		map textures/aj_lilith/ladder.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_lilith/jumppad
{
	surfaceparm nomarks

//	q3map_lightRGB 0.5 1.0 0.5
//	q3map_surfacelight 500

	qer_editorimage textures/aj_lilith/jumppad.jpg
	{
		map textures/aj_lilith/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_lilith/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.4 0.5 0 0.7
	}
}

textures/aj_lilith/fluoro1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000

	qer_editorimage textures/aj_lilith/fluoro1.jpg
	{
		map textures/aj_lilith/fluoro1.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_lilith/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 2000

	qer_editorimage textures/aj_lilith/fluoro1.jpg
	{
		map textures/aj_lilith/fluoro1.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_lilith/fluoro3
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3700

	qer_editorimage textures/aj_lilith/fluoro1.jpg
	{
		map textures/aj_lilith/fluoro1.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_lilith/fluoro4
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 7000

	qer_editorimage textures/aj_lilith/fluoro1.jpg
	{
		map textures/aj_lilith/fluoro1.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_lilith/rlamp1
{
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 5000

	qer_editorimage textures/aj_lilith/rlamp.jpg
	{
		map textures/aj_lilith/rlamp
		rgbGen identityLighting
	}
}

textures/aj_lilith/pillarlight
{
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 500

	qer_editorimage textures/aj_lilith/pillarlight.jpg
	{
		map textures/aj_lilith/pillarlight
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_lilith/pillarlight_glow
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_lilith/pipe1
{
	surfaceparm nolightmap

	qer_editorimage textures/aj_lilith/pipe1.jpg
	{
		map textures/aj_lilith/pipe1
		rgbGen identityLighting
	}
}

textures/aj_lilith/wall3
{
	qer_editorimage textures/aj_lilith/wall2.jpg

	{
		map textures/aj_lilith/wall2.jpg
		rgbGen identity  // mimick engine behavior
	}
	{
		map $lightmap
		blendFunc filter
	}
}
