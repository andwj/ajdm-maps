//
// Loonies shaders
//

textures/loonies/sky
{
	skyparms env/loonies/nebulae2 2048 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 200 25

	qer_editorimage env/loonies/nebulae2_up.jpg

	// pure skybox, no cloud layers
}

textures/loonies/slime
{
	cull none
	surfaceparm slime
	surfaceparm noimpact
	surfaceparm nolightmap
//	surfaceparm trans
	deformVertexes wave 64 sin .25 .25 0 .5	

	qer_editorimage  textures/loonies/slime8.jpg
	q3map_lightimage textures/loonies/slime8.jpg
	q3map_lightSubdivide 32
	q3map_surfacelight 100

	{
		map textures/loonies/slime8.jpg
		tcmod turb .05 -0.5 0 0.02
		tcmod scroll .05 -.01
	}
	{
		map textures/loonies/slime8.jpg
		blendfunc filter
		tcmod turb .012 -0.1 0 0.04
		tcmod scroll .003 -.008
	}
}

textures/loonies/tlamp1
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 2000
	qer_editorimage textures/loonies/tlamp.tga

	{
		map textures/loonies/tlamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/loonies/tlamp_glow
		blendFunc add
	}
}

textures/loonies/tlamp2
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 4000
	qer_editorimage textures/loonies/tlamp.tga

	{
		map textures/loonies/tlamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/loonies/tlamp_glow
		blendFunc add
	}
}

textures/loonies/tlamp3
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 8000
	qer_editorimage textures/loonies/tlamp.tga

	{
		map textures/loonies/tlamp
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/loonies/tlamp_glow
		blendFunc add
	}
}

textures/loonies/fluoro1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 500

	qer_editorimage textures/loonies/fluoro1.jpg
	{
		map textures/loonies/fluoro1.jpg
		rgbGen const ( 1 1 1 )
	}
//	{
//		map textures/loonies/fluoro_glow.jpg
//		blendFunc add
//	}
}

textures/loonies/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000

	qer_editorimage textures/loonies/fluoro1.jpg
	{
		map textures/loonies/fluoro1.jpg
		rgbGen const ( 1 1 1 )
	}
//	{
//		map textures/loonies/fluoro1_glow.jpg
//		blendFunc add
//	}
}

textures/loonies/fluoro4
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1500

	qer_editorimage textures/loonies/fluoro4.jpg
	{
		map textures/loonies/fluoro4.jpg
		rgbGen const ( 1 1 1 )
	}
//	{
//		map textures/loonies/fluoro4_glow.jpg
//		blendFunc add
//	}
}

textures/loonies/telewalllow
{
	surfaceparm nomarks
	qer_editorimage textures/loonies/telewall.jpg

	{
		map textures/loonies/telewall.jpg
		rgbGen const ( 1 1 1 )
	}
	{
		map textures/loonies/telewall_glow.jpg
		rgbGen const ( 1 1 1 )
		tcMod scale 1 -1
		tcMod scroll 0 1
		blendFunc add
	}
}

textures/loonies/telewallhigh
{
	surfaceparm nomarks
	qer_editorimage textures/loonies/telewall.jpg

	{
		map textures/loonies/telewall.jpg
		rgbGen const ( 1 1 1 )
	}
	{
		map textures/loonies/telewall_glow.jpg
		rgbGen const ( 1 1 1 )
		tcMod scroll 0 1
		blendFunc add
	}
}

textures/loonies/telefloor
{
	surfaceparm nomarks
	qer_editorimage textures/loonies/telefloor.jpg
	{
		map textures/loonies/telefloor.jpg
		rgbGen const ( 1 1 1 )
	}
	{
		map textures/loonies/telefloor_glow.tga
		rgbGen wave triangle 0.4 0.6 0.33 1.0
		blendFunc add
	}
}
