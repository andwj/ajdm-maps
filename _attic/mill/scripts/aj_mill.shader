//
// Mill shaders
//

textures/mill/sky
{
	skyparms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 800 16
	q3map_globaltexture

	qer_editorimage textures/mill/sky1.jpg
	{
		map textures/mill/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/mill/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/mill/water
{
	cull none

	surfaceparm water
	surfaceparm fog
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm lightfilter

	fogParms ( 0.07 0.07 0.99 ) 2000

	qer_editorimage textures/mill/water.tga

	{
		map textures/mill/water.tga
		blendfunc add
		rgbGen const ( 0.401961 0.401961 0.401961 )
		tcMod scroll -0.029 -0.015
	}
	{
		map textures/mill/water.tga
		blendfunc add
		rgbGen const ( 0.401961 0.401961 0.401961 )
		tcMod scroll 0.013 -0.027
	}
}

textures/mill/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_surfacelight 50
	tesssize 64

	qer_editorimage textures/mill/tele1.jpg
	{
		map textures/mill/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/mill/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/mill/pentagram
{
	surfaceparm nodamage
	surfaceparm nomarks

	{
		map textures/mill/pentagram.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/mill/pentagram_glow.jpg
		rgbGen wave sin 0.3 0.2 0 0.4
		blendFunc add
	}
}

textures/mill/fluoro
{
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000

	qer_editorimage textures/mill/fluoro.jpg
	{
		map textures/mill/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/mill/fluoro_glow.jpg
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}
