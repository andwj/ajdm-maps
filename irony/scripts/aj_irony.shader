//
// Irony shaders
//

textures/aj_irony/sky
{
	skyParms full 640 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_sunExt   0.5 0.75 1 25 0 90  4 16
	q3map_lightRGB 0.9 1 1
	q3map_skylight 40 16
	q3map_globaltexture

	qer_editorimage textures/aj_irony/sky_clouds.tga
	{

		map textures/aj_irony/sky_stars
		tcMod scale 2 2
	}
	{
		map textures/aj_irony/sky_clouds
		tcMod scroll 0.02 0.07
		blendFunc add
	}
}

textures/aj_irony/lava
{
	surfaceparm lava
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	deformVertexes wave 1 sin 0.01 0.03 0 0.2 

	tessSize 32

	q3map_surfacelight 200
	q3map_lightimage textures/aj_irony/lavahell2.tga
	q3map_lightSubdivide 8

	qer_editorimage  textures/aj_irony/lavahell2.tga
	{
		map textures/aj_irony/lavahell2.tga
		tcMod scroll -0.01 0.005
		tcMod turb 0.5 0.1 0 0.2
	}
}

textures/aj_irony/lamp1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 4000

	qer_editorimage textures/aj_irony/lamp.jpg
	{
		map textures/aj_irony/lamp
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_irony/lamp_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_irony/lamp2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 2500

	qer_editorimage textures/aj_irony/lamp.jpg
	{
		map textures/aj_irony/lamp
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_irony/lamp_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_irony/lamp3
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 10000

	qer_editorimage textures/aj_irony/lamp.jpg
	{
		map textures/aj_irony/lamp
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_irony/lamp_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_irony/cross
{
	q3map_lightRGB 1 0.7 0.4
	q3map_surfacelight 1000

	qer_editorimage textures/aj_irony/cross.jpg
	{
		map textures/aj_irony/cross
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_irony/cross_glow
		rgbGen wave sin 0.3 0.3 0 0.5
		blendFunc add
	}
}

textures/aj_irony/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 0.4 0.7 1.0
	q3map_surfacelight 500

	qer_editorimage textures/aj_irony/tele1.jpg
	{
		map textures/aj_irony/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_irony/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_irony/jumppad
{
	surfaceparm nomarks

	q3map_lightRGB 1 0.7 0.4
	q3map_surfacelight 700

	qer_editorimage textures/aj_irony/jumppad.jpg
	{
		map textures/aj_irony/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_irony/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.6 0.4 0 1.0
	}
}

textures/aj_irony/bottom_blue
{
	{
		map textures/aj_irony/bottom_blue
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_irony/bottom_blue_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_irony/tlamp1
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 8000
	qer_editorimage textures/aj_irony/tlamp.tga

	{
		map textures/aj_irony/tlamp
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_irony/tlamp_glow
		blendFunc add
	}
}

textures/aj_irony/tlamp2
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_irony/tlamp.tga

	{
		map textures/aj_irony/tlamp
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_irony/tlamp_glow
		blendFunc add
	}
}
