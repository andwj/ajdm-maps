//
// Healer shaders
//

textures/aj_healer/sky
{
	skyParms full 800 -

	surfaceparm sky
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_globaltexture
	q3map_sun 1.00 0.80 0.60 60 0 51
	q3map_lightRGB 1 0.95 0.9
	q3map_skylight 200 9

	qer_editorimage textures/aj_healer/sky1.jpg
	{
		map textures/aj_healer/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_healer/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/aj_healer/lava
{
	cull disable

	surfaceparm lava
	surfaceparm trans
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm lightfilter

	deformVertexes wave 256 sin 0 4.5 0 0.4
	tessSize 128

	q3map_lightRGB 1.0 0.2 0.1
	q3map_surfacelight 200
	q3map_globaltexture

	qer_editorImage textures/aj_healer/lavafloor.tga
	{
		map textures/aj_healer/lavafloor
		tcMod turb 0 0.1 0 0.1
		tcMod scroll 0.1 0.2
	}
	{
		map textures/aj_healer/lavafloor
		blendfunc add
		tcMod turb 0 0.2 0 0.1
		tcMod scroll -0.05 -0.02
	}
	{
		map textures/aj_healer/lavafloor
		blendfunc add
		tcMod turb 1 -0.3 0 0.07
		tcMod scroll 0.08 -0.08
	}
}

textures/aj_healer/lavafall
{
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1.0 0.2 0.1
	q3map_surfacelight 100
	q3map_globaltexture

	qer_editorImage textures/aj_healer/lavafloor.tga
	{
		map textures/aj_healer/lavafloor
		tcMod scroll  0.100 -0.400
	}
	{
		map textures/aj_healer/lavafloor
		tcMod scroll -0.082 -0.159
		blendfunc add
	}
}

textures/aj_healer/picture1
{
	surfaceparm nomarks

	// q3map_lightRGB 1.0 0.7 .3
	// q3map_surfacelight 1200

	qer_editorimage textures/aj_healer/hazard.tga
	{
		map textures/aj_healer/hazard.tga
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_healer/hazard_glow.tga
		blendFunc add
		rgbGen wave sin 0.7 0.2 0 0.5
	}
}

textures/aj_healer/grate2
{
	surfaceparm trans

	qer_editorimage textures/aj_healer/grate2.tga
	{
		map textures/aj_healer/grate2.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_healer/pillarlight
{
	q3map_lightRGB 0.82 0.90 1.00
	q3map_surfacelight 500

	qer_editorimage textures/aj_healer/pillarlight.jpg
	{
		map textures/aj_healer/pillarlight
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_healer/pillarlight_glow
		rgbGen const ( 0.5 0.5 0.5 )
		blendfunc add
	}
}

textures/aj_healer/picturelight
{
	q3map_lightRGB 0.82 0.90 1.00
	q3map_surfacelight 2100

	qer_editorimage textures/aj_healer/pillarlight.jpg
	{
		map textures/aj_healer/pillarlight
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_healer/pillarlight_glow
		rgbGen const ( 0.5 0.5 0.5 )
		blendfunc add
	}
}

textures/aj_healer/fluoro4
{
	q3map_lightRGB 0.9 0.9 1
	q3map_surfacelight 1000

	qer_editorimage textures/aj_healer/fluoro4.jpg
	{
		map textures/aj_healer/fluoro4
		rgbGen const ( 0.55 0.55 0.55 )
	}
}

textures/aj_healer/rlamp1
{
	q3map_lightRGB 0.9 0.9 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_healer/rlamp.tga

	{
		map textures/aj_healer/rlamp.tga
		rgbGen const ( 0.6 0.6 0.6 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_healer/rlamp_glow.tga
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc add
	}
}
