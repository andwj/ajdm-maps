//
// FiveSteps shaders
//

textures/aj_five/sky
{
	skyParms - 512 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_skylight 350 20
	q3map_globaltexture

	qer_editorimage textures/aj_five/sky1.jpg
	{
		map textures/aj_five/sky1.jpg
		tcMod scroll 0.045 0.055
		tcMod scale 3 2
		depthWrite
	}
	{
		map textures/aj_five/sky2.jpg
		blendfunc add
		tcMod scroll 0.02 0.02
		tcMod scale 3 3
	}
}

textures/aj_five/banner
{
	cull none

	surfaceparm nomarks
	surfaceparm alphashadow

	q3map_tessSize 32
	deformVertexes wave 256 sin 0 3 0 0.4

	qer_editorimage textures/aj_five/banner1.tga
	{
		map textures/aj_five/banner1.tga
		rgbGen identity
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_five/fancycable
{
	surfaceparm nomarks
	surfaceparm metalsteps

	q3map_surfacelight 25
	q3map_lightimage textures/aj_five/fancycable_glow.jpg
	qer_editorimage  textures/aj_five/fancycable.jpg

	{
		map textures/aj_five/fancycable.jpg
		rgbGen identity
	}
	{
		map $lightmap
		rgbGen identity
		blendFunc filter
	}
	{
		map textures/aj_five/fancycable_glow.jpg
		tcMod scroll 0 1
		blendFunc add
	}
}

textures/aj_five/jumppad1
{
	surfaceparm nomarks

	qer_editorimage textures/aj_five/jumppad1.jpg
	{
		map textures/aj_five/jumppad1.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_five/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.5 0.5 0 1.0
	}
}

textures/aj_five/jumppad2
{
	surfaceparm nomarks

	qer_editorimage textures/aj_five/jumppad2.jpg
	{
		map textures/aj_five/jumppad2.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_five/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.5 0.5 0 1.0
	}
}

textures/aj_five/grate
{
	surfaceparm trans

	qer_editorimage textures/aj_five/grate.tga
	{
		map textures/aj_five/grate.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_five/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000
	qer_editorimage textures/aj_five/fluoro.jpg
	{
		map textures/aj_five/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_five/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_five/fluoro4
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 2000
	qer_editorimage textures/aj_five/fluoro.jpg
	{
		map textures/aj_five/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_five/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_five/fluoro6
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_five/fluoro.jpg
	{
		map textures/aj_five/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_five/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_five/squarelight
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000
	qer_editorimage textures/aj_five/sqlight.jpg
	{
		map textures/aj_five/sqlight.jpg
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_five/sqlight_glow.jpg
		blendFunc add
	}
}

textures/aj_five/redlight
{
	q3map_lightRGB 1 0.5 0.5
	q3map_surfacelight 4000
	qer_editorimage textures/aj_five/redlight.jpg
	{
		map textures/aj_five/redlight.jpg
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_five/redlight_glow.jpg
		blendFunc add
	}
}

textures/aj_five/glass
{
	cull disable
	surfaceparm trans
	surfaceparm nolightmap

	qer_trans 0.4
	qer_editorimage textures/aj_five/glassfx2.tga
	{
		map textures/aj_five/glassfx2.tga
		tcGen environment
		blendFunc add
	}
}

textures/aj_five/computer1
{
	qer_editorimage textures/aj_five/comp_text.tga

	{
		map textures/aj_five/comp_text
		rgbGen const ( 0.1 0.4 0.1 )
		tcMod scroll 0 0.17
	}
	{
		map textures/aj_five/comp_frame
		rgbGen const ( 0.3 0.3 0.3 )
		blendFunc blend
	}
}

textures/aj_five/trim_nonsolid
{
	surfaceparm nonsolid
	qer_editorimage textures/aj_five/trim.jpg
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_five/trim.jpg
		rgbGen identity
		blendFunc filter
	}
}

textures/aj_five/telebeam
{
	cull disable

	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nomarks
	surfaceparm nolightmap

	qer_editorimage textures/aj_five/telebeam.jpg
	{
		map textures/aj_five/telebeam
		tcMod scale 1.0 4.0
		blendfunc add
	}
}

textures/aj_five/teleback
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nodlight

	q3map_lightRGB 1.0 0.5 1.0
	q3map_surfacelight 150

	qer_editorimage textures/aj_five/teleback.jpg
	{
		map textures/aj_five/teleback
	}
	{
		map textures/aj_five/teletwirl
		tcMod rotate -540
		blendFunc blend
	}
	{
		clampMap textures/aj_five/teleswirl
		tcMod rotate 573
		blendFunc add
	}
	{
		clampMap textures/aj_five/teleswirl
		tcMod rotate 573
		tcMod stretch sin 0.52 0 0 0
		blendFunc add
	}
}

textures/aj_five/telefront
{
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm noimpact
	surfaceparm nomarks
	surfaceparm nolightmap
	surfaceparm nodlight

	qer_editorimage textures/aj_five/telepulchr.tga
	{
		map textures/aj_five/teleswirl
		tcMod rotate -320
		blendFunc add
	}
//	{
//		map textures/aj_five/telepulchr
//		rgbGen wave sin 0.7 0.2 0 0.5
//		blendFunc blend
//	}
}
