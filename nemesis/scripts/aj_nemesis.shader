//
// Nemesis shaders
//

textures/aj_nemesis/sky
{
	skyParms full 756 -

	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky

	q3map_lightRGB 1 1 1
	q3map_skyLight 120 9
	q3map_sunExt  1 1 1  30 90 90  8 9
	q3map_globaltexture

	qer_editorimage textures/aj_nemesis/sky01.jpg
	{
		map textures/aj_nemesis/sky01
		tcMod scale 1 1
		tcMod scroll 0.2 0.2
		depthWrite
	}
	{
		map textures/aj_nemesis/sky02
		blendfunc add
		tcMod scale 2 2
		tcMod scroll 0.05 0.05
	}
}

textures/aj_nemesis/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nodlight
	surfaceparm nolightmap

	tesssize 64

	q3map_surfacelight 200

	qer_editorimage textures/aj_nemesis/tele1.jpg
	{
		map textures/aj_nemesis/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_nemesis/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_nemesis/fluoro_green
{
	q3map_lightRGB 0.5 1 0.5
	q3map_surfacelight 1000

	qer_editorimage textures/aj_nemesis/fluoro.jpg
	{
		map textures/aj_nemesis/fluoro.jpg
		rgbGen const ( 0.5 0.6 0.5 )
	}
	{
		map textures/aj_nemesis/fluoro_glow.jpg
		rgbGen const ( 0.5 1.0 0.0 )
		blendFunc add
	}
}

textures/aj_nemesis/fluoro_orange
{
	q3map_lightRGB 1 0.5 0.0
	q3map_surfacelight 2500

	qer_editorimage textures/aj_nemesis/fluoro.jpg
	{
		map textures/aj_nemesis/fluoro.jpg
		rgbGen const ( 0.6 0.55 0.5 )
	}
	{
		map textures/aj_nemesis/fluoro_glow.jpg
		rgbGen const ( 1.0 0.5 0.0 )
		blendFunc add
	}
}

textures/aj_nemesis/fluoro_white
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000

	qer_editorimage textures/aj_nemesis/fluoro.jpg
	{
		map textures/aj_nemesis/fluoro.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_nemesis/fluoro_glow.jpg
		blendFunc add
	}
}

textures/aj_nemesis/bfluoro
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1500

	qer_editorimage textures/aj_nemesis/bfluoro.jpg
	{
		map textures/aj_nemesis/bfluoro.jpg
		rgbGen const ( 0.6 0.6 0.6 )
	}
}

textures/aj_nemesis/tlite4
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 4000

	qer_editorimage textures/aj_nemesis/tlite.jpg
	{
		map textures/aj_nemesis/tlite.jpg
	}
	{
		map textures/aj_nemesis/tlite_glow.jpg
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_nemesis/tlite10
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 9000

	qer_editorimage textures/aj_nemesis/tlite.jpg
	{
		map textures/aj_nemesis/tlite.jpg
	}
	{
		map textures/aj_nemesis/tlite_glow.jpg
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_nemesis/weapon_spot
{
	qer_editorimage textures/aj_nemesis/cross1.tga
	{
		map textures/aj_nemesis/cross1.tga
		rgbGen const ( 0.85 0.85 0.85 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_nemesis/cross1_glow.tga
		rgbGen wave sin 0.5 0.3 0 0.35
		blendFunc add
	}
}

textures/aj_nemesis/glass
{
	surfaceparm trans       
	surfaceparm nomarks

	qer_trans 0.5
	qer_editorimage textures/aj_nemesis/tinfx3.jpg
	{
		map textures/aj_nemesis/tinfx3
		tcGen environment
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_nemesis/banner
{
	cull none
	surfaceparm nomarks
//	surfaceparm alphashadow

	q3map_tessSize 32
	deformVertexes wave 256 sin 0 3 0 0.4

	qer_editorimage textures/aj_nemesis/banner.tga
	{
		map textures/aj_nemesis/banner.tga
		rgbGen identity
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_nemesis/flower
{
	q3map_lightRGB 1 0.3 0.3
	q3map_surfacelight 1000

	qer_editorimage textures/aj_nemesis/flower.jpg
	{
		map textures/aj_nemesis/flower.jpg
		rgbGen identity
	}
	{
		map $lightmap
		rgbGen identity
		blendFunc filter
	}
}

textures/aj_nemesis/jumppad
{
	surfaceparm nomarks

	q3map_lightRGB 1 1 0.3
	q3map_surfacelight 120

	qer_editorimage textures/aj_nemesis/jumppad.jpg
	{
		map textures/aj_nemesis/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_nemesis/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.6 0.4 0 1.0
	}
}

textures/aj_nemesis/nemesis_online
{
	nopicmip

	surfaceparm nomarks
	surfaceparm nodlight
	surfaceparm nolightmap

	qer_editorimage textures/aj_nemesis/online.tga
	{
		map textures/aj_nemesis/online.tga
		rgbGen wave sin 0.8 0.3 0 0.5
	}
	{
		map textures/aj_nemesis/glassfx2.tga
		blendFunc GL_ONE GL_ONE
		tcGen environment
	}
}

textures/aj_nemesis/slime
{
	surfaceparm slime
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 0.3 1 0.3
	q3map_lightSubdivide 32
	q3map_surfacelight 200

	qer_editorimage textures/aj_nemesis/slime.jpg
	{
		map textures/aj_nemesis/slime.jpg
//		rgbGen const ( 0.5 0.5 0.5 )
		tcmod scroll .05 -.01
	}
	{
		map textures/aj_nemesis/slime.jpg
//		rgbGen const ( 0.5 0.5 0.5 )
		tcmod scroll .032 .014
		blendfunc add
	}
}

textures/aj_nemesis/foggy
{
	fogparms ( 0.458824 0.862745 0.411765 ) 333

	surfaceparm fog
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap

	{
		map textures/aj_nemesis/fogcloud.jpg
		blendfunc filter
		tcMod turb 0.5 -0.4 0.5 0.1
	}
}
