//
// Junction shaders
//

textures/aj_junction/sky
{
	skyParms - 512 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_sun 1.0 0.8 0.6  25  150 60
	q3map_lightRGB 1 0.8 0.6
	q3map_skylight 120 16
	q3map_globaltexture

	qer_editorimage textures/aj_junction/sky1.jpg
	{
		map textures/aj_junction/sky1
		tcMod scroll 0.026 0.036
		depthWrite
	}
	{
		map textures/aj_junction/sky1
		tcMod scroll 0.02 0.02
		tcMod scale 2 2
		blendFunc add
	}
}

textures/aj_junction/lava
{
	cull none

	surfaceparm lava
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nodrop

	deformVertexes wave 1 sin 0.01 0.03 0 0.2

	q3map_surfaceLight 120
	q3map_lightImage textures/aj_junction/lavahell2.tga
	q3map_lightSubdivide 256

	qer_editorimage textures/aj_junction/lavahell2.tga
	qer_trans 0.5

	{
		map textures/aj_junction/lavahell2.tga
		rgbGen const ( 0.21 0.21 0.21 )
		tcMod turb 0.5 -0.4 0.5 0.1
		blendFunc add
	}
	{
		map textures/aj_junction/lavahell2.tga
		rgbGen const ( 0.21 0.21 0.21 )
		tcMod turb 0.5 -0.3 0.4 0.14
		blendFunc add
	}
}

textures/aj_junction/teleporter
{
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm noimpact
	surfaceparm nomarks
	surfaceparm nodamage

	q3map_tessSize 64

	q3map_lightRGB 1 0.25 0.25
	q3map_surfacelight 100

	qer_editorimage textures/aj_junction/telefx.jpg
	{
		map textures/aj_junction/telefx
		tcMod scroll -0.5 0.03
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_junction/telefx
		tcMod scroll 0.5 -0.05
		tcMod turb 0 .12 0 0.333
		blendFunc add
	}
}

textures/aj_junction/jumppad
{
	surfaceparm nomarks

	q3map_lightRGB 1.0 0.8 0.4
	q3map_surfacelight 300

	qer_editorimage textures/aj_junction/jumppad.jpg
	{
		map textures/aj_junction/jumppad
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_junction/jumppad_glow
		blendFunc add
		rgbGen wave sin 0.4 0.5 0 0.7
	}
}

textures/aj_junction/grate
{
	surfaceparm trans

	qer_editorimage textures/aj_junction/grate.tga
	{
		map textures/aj_junction/grate.tga
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_junction/logo
{
	surfaceparm nomarks
	surfaceparm nolightmap

	qer_editorimage textures/aj_junction/logo.jpg
	{
		map textures/aj_junction/logo
		rgbGen const ( 0.9 0.9 0.9 )
	}
}

textures/aj_junction/sqlight1
{
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 2000

	qer_editorimage textures/aj_junction/sqlight.jpg
	{
		map textures/aj_junction/sqlight
		rgbGen identityLighting
	}
}

textures/aj_junction/sqlight2
{
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 5000

	qer_editorimage textures/aj_junction/sqlight.jpg
	{
		map textures/aj_junction/sqlight
		rgbGen identityLighting
	}
}

textures/aj_junction/pillarlight
{
	q3map_lightRGB 1 0.3 0.2
	q3map_surfacelight 200

	qer_editorimage textures/aj_junction/pillarlight.jpg
	{
		map textures/aj_junction/pillarlight
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_junction/pillarlight_glow
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}
