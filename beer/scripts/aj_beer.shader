//
// Beer shaders
//

textures/aj_beer/sky
{
	skyParms - 512 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_lightRGB 1.0 1.0 1.0
	q3map_skylight 120 9
	q3map_globaltexture

	qer_editorimage textures/aj_beer/sky1.jpg
	{
		map textures/aj_beer/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_beer/sky1
		blendfunc add
		tcMod scroll 0.01 0.01
		tcMod scale 2 2
	}
}

textures/aj_beer/teleport
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 0.6 0.8 1.0
	q3map_surfacelight 200

	qer_editorimage textures/aj_beer/telefx.jpg
	{
		map textures/aj_beer/telefx
		rgbGen const ( 0.3 0.3 0.3 )
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_beer/telefx
		rgbGen const ( 0.3 0.3 0.3 )
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc add
	}
}

textures/aj_beer/deadtele
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	qer_editorimage textures/aj_beer/tek.tga

	{
		map textures/aj_beer/tek
		rgbGen const ( 1 1 1 )
	}
	{
		map textures/aj_beer/tek_glow.tga
		rgbGen const ( 1 1 1 )
		tcMod scroll 0 0.5
		blendFunc filter
	}
}

textures/aj_beer/drinkbeer
{
	nopicmip

	surfaceparm nomarks
	surfaceparm nodlight
	surfaceparm nolightmap

	{
		map textures/aj_beer/drinkbeer.jpg
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/jaildrblue
{
	surfaceparm nomarks

	q3map_lightRGB 0.6 0.8 1.0
	q3map_surfacelight 500

	qer_editorimage textures/aj_beer/jaildrblue.tga
	{
		map textures/aj_beer/jaildrblue.tga
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_beer/jaildrblue_glow.tga
		blendFunc add
		rgbGen wave sin 0.8 0.2 0  0.25
	}
}

textures/aj_beer/lglass
{
	surfaceparm trans
	surfaceparm nomarks
	surfaceparm nolightmap

	qer_editorimage textures/aj_beer/glassfx2.jpg
	qer_trans 0.5

	{
		map textures/aj_beer/glassfx2.jpg
		rgbGen const ( 0.25 0.25 0.25 )
		tcGen environment
		tcMod scale 1 2
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_beer/bluefog
{
	fogParms ( 0 .5 1 ) 256

	surfaceparm fog
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap
	surfaceparm nodlight

	qer_editorimage textures/aj_beer/fogcloud.jpg
	{
		map textures/aj_beer/fogcloud.jpg
		blendfunc gl_dst_color gl_zero
		tcmod scale -.05 -.05
		tcmod scroll .01 -.01
		rgbgen identity
	}
	{
		map textures/aj_beer/fogcloud.jpg
		blendfunc gl_dst_color gl_zero
		tcmod scale .05 .05
		tcmod scroll .01 -.01
		rgbgen identity
	}
}

textures/aj_beer/olight1
{
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 500

	qer_editorimage textures/aj_beer/olight.tga
	{
		map textures/aj_beer/olight.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/olight2
{
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 1500

	qer_editorimage textures/aj_beer/olight.tga
	{
		map textures/aj_beer/olight.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/jumppad
{
	surfaceparm nodamage
	polygonoffset

	qer_editorimage textures/aj_beer/jumppad2.tga
	{
		map textures/aj_beer/jumppad2.tga
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_beer/jumpswirl_3.tga
		tcMod rotate 108
		rgbGen const ( 0.5 0.5 0.5 )
		blendFunc add
	}
}

textures/aj_beer/gratelamp
{
	q3map_lightRGB 1.0 0.7 0.4
	q3map_surfacelight 15000

	qer_editorimage textures/aj_beer/tlamp1.tga

	{
		map textures/aj_beer/tlamp1.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/minorlamp
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 8500

	qer_editorimage textures/aj_beer/tlamp2.jpg

	{
		map textures/aj_beer/tlamp2
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/cyanlamp
{
	q3map_lightRGB 0.0 0.5 1.0
	q3map_surfacelight 12000

	qer_editorimage textures/aj_beer/tlamp3.jpg

	{
		map textures/aj_beer/tlamp3
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/sqlight1
{
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 4500

	qer_editorimage textures/aj_beer/sqlight.jpg
	{
		map textures/aj_beer/sqlight
		rgbGen const ( 0.5 0.5 0.5 )
	}
}

textures/aj_beer/sqlight2
{
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_lightRGB 1 1 1
	q3map_surfacelight 2000

	qer_editorimage textures/aj_beer/sqlight.jpg
	{
		map textures/aj_beer/sqlight
		rgbGen const ( 0.5 0.5 0.5 )
	}
}
