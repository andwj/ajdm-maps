//
// Osiris shaders
//

textures/aj_osiris/sky
{
	skyParms - 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	q3map_lightRGB 1 1 1
	q3map_skylight 500 16

	qer_editorimage textures/aj_osiris/sky1.jpg
	{
		map textures/aj_osiris/sky1
		tcMod scroll 0.016 0.006
		depthWrite
	}
	{
		map textures/aj_osiris/sky1
		blendfunc add
		tcMod scroll 0.021 -0.009
		tcMod scale 2 2
	}
}

textures/aj_osiris/teleporter
{
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nodlight
	surfaceparm nolightmap

	q3map_surfacelight 40
	tesssize 64

	qer_editorimage textures/aj_osiris/tele1.jpg
	{
		map textures/aj_osiris/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_osiris/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_osiris/jumppad
{
	surfaceparm nodamage
	polygonoffset

	{
		map textures/aj_osiris/jumppad.tga
		rgbGen const ( 0.5 0.5 0.5 )
	}
	{
		map textures/aj_osiris/jumppad_glow.tga
		tcMod stretch sine 1.2 0.2 0 0.77
		rgbGen const ( 0.4 0.4 0.4 )
		blendFunc add
	}
	{
		map textures/aj_osiris/jumppad_outer.tga
		rgbGen const ( 0.5 0.5 0.5 )
		alphaFunc GE128
	}
}

textures/aj_osiris/torchwood
{
	cull disable
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nomarks

	{
		map textures/aj_osiris/torchwood.jpg
	}
	{
		map $lightmap
		blendFunc filter
	}
}

textures/aj_osiris/blueflame
{
	cull disable

	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap
	surfaceparm nomarks

	qer_editorimage textures/aj_osiris/b_flame1.tga
	{
		animMap 8 textures/aj_osiris/b_flame1.tga textures/aj_osiris/b_flame3.tga textures/aj_osiris/b_flame5.tga textures/aj_osiris/b_flame7.tga
		blendFunc blend
	}
}
