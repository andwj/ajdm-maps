//
// Trial shaders
//

textures/aj_trial/sky
{
	skyParms full 640 -
	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap

//	q3map_lightRGB 1 1 1
//	q3map_skylight 20 9
	q3map_sun 0.7 0.8 1.0  50  45 90
	q3map_globaltexture

	qer_editorimage textures/aj_trial/sky_clouds.tga

	{
		map textures/aj_trial/sky_stars
		tcMod scale 3 3
	}
	{
		map textures/aj_trial/sky_clouds
		tcMod scroll 0.02 0.07
		blendFunc add
	}
}

textures/aj_trial/water
{
	cull none

	surfaceparm water
	surfaceparm trans
	surfaceparm nomarks

//	surfaceparm fog
//	fogParms ( 0.07 0.07 0.99 ) 2000

	qer_editorimage textures/aj_trial/water.tga

	{
		map textures/aj_trial/water.tga
		rgbGen const ( 0.12 0.12 0.12 )
		tcMod scroll -0.029 -0.015
		blendfunc add
	}
	{
		map textures/aj_trial/water.tga
		rgbGen const ( 0.12 0.12 0.12 )
		tcMod scroll 0.013 -0.027
		blendfunc add
	}
}

textures/aj_trial/banner
{
	surfaceparm nomarks
	surfaceparm alphashadow

	q3map_tessSize 32
	deformVertexes wave 256 sin 0 3 0 0.4

	qer_editorimage textures/aj_trial/banner.tga
	{
		map textures/aj_trial/banner.tga
		rgbGen identity
		alphaFunc GE128
		depthWrite
	}
	{
		map $lightmap
		blendFunc filter
		depthFunc equal
	}
}

textures/aj_trial/jumppad
{
	surfaceparm nomarks

//	q3map_surfacelight 150
//	q3map_lightRGB 1 0.7 0.4

	qer_editorimage textures/aj_trial/jumppad.jpg
	{
		map textures/aj_trial/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_trial/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.4 0.4 0 1.0
	}
}

textures/aj_trial/panlight
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 700

	qer_editorimage textures/aj_trial/xlight6.jpg
	{
		map textures/aj_trial/xlight6.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_trial/xlight1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1500

	qer_editorimage textures/aj_trial/xlight6.jpg
	{
		map textures/aj_trial/xlight6.jpg
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_trial/tlamp1
{
	q3map_surfacelight 1000
	q3map_lightRGB 1 1 1
	qer_editorimage textures/aj_trial/drkmtl_light.tga

	{
		map textures/aj_trial/drkmtl_light
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_trial/drkmtl_light_glow
		blendFunc add
		rgbGen const ( 0.7 0.7 0.7 )
	}
}

textures/aj_trial/sqlamp1
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 4000

	qer_editorimage textures/aj_trial/sqlamp2.jpg
	{
		map textures/aj_trial/sqlamp2
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_trial/sqlamp2_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_trial/sqlamp2
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 2500

	qer_editorimage textures/aj_trial/sqlamp2.jpg
	{
		map textures/aj_trial/sqlamp2
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_trial/sqlamp2_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}

textures/aj_trial/sqlamp3
{
	q3map_lightRGB 0.9 1 1
	q3map_surfacelight 800

	qer_editorimage textures/aj_trial/sqlamp2.jpg
	{
		map textures/aj_trial/sqlamp2
		rgbGen const ( 0.4 0.4 0.4 )
	}
	{
		map textures/aj_trial/sqlamp2_glow
		rgbGen const ( 0.2 0.2 0.2 )
		blendFunc add
	}
}
