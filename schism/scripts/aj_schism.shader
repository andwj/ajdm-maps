//
// Schism shaders
//

textures/aj_schism/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact
	surfaceparm nodamage

	q3map_tessSize 64

	q3map_lightRGB 1 0.3 0.3
	q3map_surfacelight 400

	qer_editorimage textures/aj_schism/tele1.jpg
	{
		map textures/aj_schism/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_schism/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_schism/pillarlight
{
	qer_editorimage textures/aj_schism/pillarlight.jpg
	{
		map textures/aj_schism/pillarlight
		rgbGen const ( 0.3 0.3 0.3 )
	}
	{
		map textures/aj_schism/pillarlight_glow
		rgbGen const ( 0.5 0.5 0.5 )
		blendfunc add
	}
}

textures/aj_schism/light1
{
	qer_editorimage textures/aj_schism/light3.jpg
	q3map_surfacelight 1700
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_schism/light3.tga
		rgbGen const ( 0.8 0.8 0.8 )
		blendFunc filter
	}
	{
		map textures/aj_schism/light3_glow.tga
		blendfunc add
	}
}

textures/aj_schism/light2
{
	qer_editorimage textures/aj_schism/light3.jpg
	q3map_surfacelight 3500
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_schism/light3.tga
		rgbGen const ( 0.8 0.8 0.8 )
		blendFunc filter
	}
	{
		map textures/aj_schism/light3_glow.tga
		blendfunc add
	}
}

textures/aj_schism/light3
{
	qer_editorimage textures/aj_schism/light3.tga
	q3map_surfacelight 8000
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_schism/light3.tga
		rgbGen const ( 0.8 0.8 0.8 )
		blendFunc filter
	}
	{
		map textures/aj_schism/light3_glow.tga
		blendfunc add
	}
}

textures/aj_schism/biglight1
{
	qer_editorimage textures/aj_schism/biglight.jpg
	q3map_surfacelight 4000
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_schism/biglight
		rgbGen identity
		blendFunc filter
	}
}

textures/aj_schism/fluoro4
{
	qer_editorimage textures/aj_schism/fluoro4.jpg
	q3map_surfacelight 3000
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/aj_schism/fluoro4
		rgbGen identity
		blendFunc filter
	}
}
