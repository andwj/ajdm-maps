//
// Elbow shaders
//

textures/aj_elbow/sky
{
	skyParms full 500 -

	surfaceparm sky
	surfaceparm nomarks
	surfaceparm noimpact
	surfaceparm nolightmap

	q3map_globaltexture
	q3map_sunExt 1.00 0.50 0.20  60  0 90  2 25
	q3map_lightRGB 1 1 1
	q3map_skylight 200 16

	qer_editorimage textures/aj_elbow/sky1.jpg

	{
		map textures/aj_elbow/sky1.jpg
		tcMod scale 2 3
		tcMod scroll .003 .01
	}
	{
		map textures/aj_elbow/sky2.jpg
		blendFunc filter
		tcMod scale 3 2
		tcMod scroll 0.01 0.025
	}
}

textures/aj_elbow/jumppad
{
	surfaceparm nomarks

	q3map_lightRGB 1.0 0.7 .3
	q3map_surfacelight 1200

	qer_editorimage textures/aj_elbow/jumppad.jpg
	{
		map textures/aj_elbow/jumppad.jpg
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_elbow/jumppad_glow.tga
		blendFunc add
		rgbGen wave sin 0.6 0.4 0 1.0
	}
}

textures/aj_elbow/teleporter
{
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

	q3map_lightRGB 0.3 0.7 1
	q3map_surfacelight 700

	qer_editorimage textures/aj_elbow/tele1.jpg
	{
		map textures/aj_elbow/tele1
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_elbow/tele1
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc GL_ONE GL_ONE
	}
}

textures/aj_elbow/fluoro1
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 1000

	qer_editorimage textures/aj_elbow/fluoro1.jpg
	{
		map textures/aj_elbow/fluoro1.jpg
		rgbGen const ( 1 1 1 )
	}
}

textures/aj_elbow/fluoro2
{
	q3map_lightRGB 1 1 1
	q3map_surfacelight 3000

	qer_editorimage textures/aj_elbow/fluoro1.jpg
	{
		map textures/aj_elbow/fluoro1.jpg
		rgbGen const ( 1 1 1 )
	}
}
