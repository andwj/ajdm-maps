//
// Keep shaders
//

textures/aj_keep/sky
{
	skyParms env/aj_keep/moon1 1024 -

	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	qer_editorimage textures/aj_keep/sky1.jpg

	q3map_lightRGB 1 0.8 0.4
	q3map_skylight 260 16

	{
		map textures/aj_keep/sky1.jpg
		blendFunc add
		tcMod scale 3 2
		tcMod scroll 0.15 0.15
	}
	{
		map textures/aj_keep/sky2.jpg
		blendFunc filter
		tcMod scale 3 3
		tcMod scroll 0.05 0.05
	}
}

textures/aj_keep/lava1
{
	cull none

	surfaceparm lava
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm nodrop

	tessSize 128

	fogparms ( 0.929412 0.347059 0.047216 ) 612
	surfaceparm fog
	surfaceparm lightfilter

	q3map_surfacelight 600
	q3map_lightimage textures/aj_keep/lavahell2.tga
	qer_editorimage  textures/aj_keep/lavahell2.tga
	qer_trans 0.5

	{
		map textures/aj_keep/lavahell2.tga
		rgbGen const ( 0.15 0.15 0.15 )
		tcMod turb 0.5 -0.4 0.5 0.1
		blendFunc add
	}
	{
		map textures/aj_keep/lavahell2.tga
		rgbGen const ( 0.15 0.15 0.15 )
		tcMod turb 0.4 -0.4 0.7 0.1
		blendFunc add
	}
}

textures/aj_keep/lava2
{
	cull none
	tessSize 128

	surfaceparm lava
	surfaceparm trans
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks

	fogparms ( 0.929412 0.347059 0.047216 ) 612
	surfaceparm fog
	surfaceparm lightfilter

	q3map_surfacelight 300
	q3map_lightimage textures/aj_keep/lavahell2.tga
	qer_editorimage  textures/aj_keep/lavahell2.tga

	{
		map textures/aj_keep/lavahell2.tga
		rgbGen const ( 0.15 0.15 0.15 )
		tcMod turb 0.5 -0.4 0.5 0.1
		blendFunc add
	}
	{
		map textures/aj_keep/lavahell2.tga
		rgbGen const ( 0.15 0.15 0.15 )
		tcMod turb 0.4 -0.4 0.7 0.1
		blendFunc add
	}
}

textures/aj_keep/teleporter
{
	surfaceparm trans
	surfaceparm nodlight
	surfaceparm nolightmap
	surfaceparm noimpact

	tesssize 64

//	q3map_lightRGB 0.3 0.7 1
//	q3map_surfacelight 900

	qer_editorimage textures/aj_keep/telefx1.tga
	{
		map textures/aj_keep/telefx1
		rgbGen identity
		tcMod scroll -0.02 0.5
		tcMod turb 0 .12 0 0.333
	}
	{
		map textures/aj_keep/telefx1
		rgbGen identity
		tcMod scroll 0.01 -0.5
		tcMod turb 0 .12 0 0.333
		blendFunc add
	}
}

textures/aj_keep/weaponspot
{
	qer_editorimage textures/aj_keep/cross1.tga
	{
		map textures/aj_keep/cross1.tga
		rgbGen const ( 0.85 0.85 0.85 )
	}
	{
		map $lightmap
		blendFunc filter
	}
	{
		map textures/aj_keep/cross1_glow.tga
		rgbGen wave sin 0.5 0.3 0 0.35
		blendFunc add
	}
}

textures/aj_keep/hanglamp
{
	surfaceparm nonsolid
	surfaceparm nolightmap
	surfaceparm nomarks

	qer_editorimage textures/aj_keep/hanglamp.tga

	{
		map textures/aj_keep/hanglamp.tga
	}
}
